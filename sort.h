#include <stdio.h>
#include "array.h"

extern array some_values ;

// fait remonter une bulle jusqu'à la case k
void bulle (int max_index) ;

// tri (décroissant) du tableau some_values par la méthode du tri à bulle 
void trier (void);
