#include <stdio.h>
#include <stdlib.h> 
#include "array.h"
#include "io.h"
#include "fill_array.h"
#include "sort.h"

array some_values; // tableau d'entiers avec sa taille

int main (int argc, char ** argv)	
{
	if (argc < 3 ){ 
		puts("usage : main <nb elems> <max val>");
		return 1;
	}
	
	some_values.taille = atoi (argv[1]); // lecture du 1er argument
	int max_value = atoi (argv[2]); // lecture du 2eme argument
	
	some_values.valeurs = 0; // initialisation du pointeur
	some_values.valeurs = malloc (some_values.taille * sizeof *some_values.valeurs); // allocation du tableau

	remplir (max_value); // remplissage aléatoire du tableau

	puts("tableau non trié :");
	affiche();

	trier();

	puts("tableau trié :");
	affiche();
	
	return 0;	
}
