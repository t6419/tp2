#include <stdlib.h>
#include "array.h"

extern array some_values ;

// tire un entier aléatoire entre 0 et m
int get_random_value (int max_value);

// remplit some_values avec des valeurs aléatoires
void fill (int max_value);
